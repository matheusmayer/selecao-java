package com.example.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;


import lombok.Data;

@Data
final public class TabelaComercialCombustivelDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private String regiao;
	private String estado;
	private String municipio;
	private String revenda;
	private String produto;
	private LocalDate dataColeta;
	private BigDecimal valorCompra;
	private BigDecimal valorVenda;
	private String unidadeMedida;
	private String bandeira;
	
	public TabelaComercialCombustivelDTO() {}
	
	public TabelaComercialCombustivelDTO(String regiao, String estado, String municipio, String revenda, String produto,
			LocalDate dataColeta, BigDecimal valorCompra, BigDecimal valorVenda, String unidadeMedida,
			String bandeira) {
		this.regiao = regiao;
		this.estado = estado;
		this.municipio = municipio;
		this.revenda = revenda;
		this.produto = produto;
		this.dataColeta = dataColeta;
		this.valorCompra = valorCompra;
		this.valorVenda = valorVenda;
		this.unidadeMedida = unidadeMedida;
		this.bandeira = bandeira;
	}

	public String getRegiao() {
		return regiao;
	}
	public String getEstado() {
		return estado;
	}
	public String getMunicipio() {
		return municipio;
	}
	public String getRevenda() {
		return revenda;
	}
	public String getProduto() {
		return produto;
	}
	public LocalDate getDataColeta() {
		return dataColeta;
	}
	public BigDecimal getValorCompra() {
		return valorCompra;
	}
	public BigDecimal getValorVenda() {
		return valorVenda;
	}
	public String getUnidadeMedida() {
		return unidadeMedida;
	}
	public String getBandeira() {
		return bandeira;
	}

	@Override
	public String toString() {
		return "TabelaComercialCombustivelDTO [regiao=" + regiao + ", estado=" + estado + ", municipio=" + municipio
				+ ", revenda=" + revenda + ", produto=" + produto + ", dataColeta=" + dataColeta + ", valorCompra="
				+ valorCompra + ", valorVenda=" + valorVenda + ", unidadeMedida=" + unidadeMedida + ", bandeira="
				+ bandeira + "]";
	}
}
