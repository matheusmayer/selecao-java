package com.example.api.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.api.model.Bandeira;

public interface BandeiraRepositoryInterface extends JpaRepository<Bandeira, Long>{
	public Bandeira findOneByNome(String nome);
}
