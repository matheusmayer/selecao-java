package com.example.api.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.api.model.Produto;

public interface ProdutoRepositoryInterface extends JpaRepository<Produto, Long>{
	public Produto findOneByNome(String nome);

}
