package com.example.api.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.api.model.LevantamentoComercialCombustivel;

public interface LevantamentoComercialCombustivelInterface extends JpaRepository<LevantamentoComercialCombustivel, Long> {

}
