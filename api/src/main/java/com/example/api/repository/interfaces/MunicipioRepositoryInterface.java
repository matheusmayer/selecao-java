package com.example.api.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.api.model.Municipio;

public interface MunicipioRepositoryInterface extends JpaRepository<Municipio, Long>{
	public Municipio findOneByNome(String nome);

}
