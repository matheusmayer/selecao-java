package com.example.api.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.api.model.Estado;

public interface EstadoRepositoryInterface extends JpaRepository<Estado, Long>{
	public Estado findOneBySigla(String sigla);
}
