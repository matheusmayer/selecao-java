package com.example.api.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.api.model.Regiao;

public interface RegiaoRepositoryInterface extends JpaRepository<Regiao, Long>{
	public Regiao findOneBySigla(String sigla);
}
