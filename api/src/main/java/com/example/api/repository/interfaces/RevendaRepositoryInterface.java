package com.example.api.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.api.model.Revenda;

public interface RevendaRepositoryInterface extends JpaRepository<Revenda, Long>{
	public Revenda findOneByNome(String nome);

}
