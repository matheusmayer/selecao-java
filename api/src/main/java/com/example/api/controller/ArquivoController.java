package com.example.api.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.api.dto.TabelaComercialCombustivelDTO;
import com.example.api.service.ArquivoService;
import com.example.api.service.BandeiraService;
import com.example.api.service.EstadoService;
import com.example.api.service.MunicipioService;
import com.example.api.service.ProdutoService;
import com.example.api.service.RegiaoService;
import com.example.api.service.RevendaService;

@RestController
@RequestMapping("/arquivos")
public class ArquivoController {

	@Autowired()
	private ArquivoService arquivoService;

	@Autowired()
	private RegiaoService regiaoService;
	
	@Autowired()
	private EstadoService estadoService;
	
	@Autowired()
	private MunicipioService municipioService;
	
	@Autowired()
	private BandeiraService bandeiraService;
	
	@Autowired()
	private ProdutoService produtoService;
	
	@Autowired()
	private RevendaService revendaService;
	
	@PostMapping
	public ResponseEntity<String> importar(@RequestParam("file") MultipartFile file) throws IOException {
		List<TabelaComercialCombustivelDTO> dadosCombustivelDTO = arquivoService.extrairDados(file);
		regiaoService.salvarTodos(regiaoService.extrairRegioes(dadosCombustivelDTO));
		estadoService.salvarTodos(estadoService.extrairEstados(dadosCombustivelDTO));
		municipioService.salvarTodos(municipioService.extrairMunicipios(dadosCombustivelDTO));
		bandeiraService.salvarTodos(bandeiraService.extrairBandeiras(dadosCombustivelDTO));
		produtoService.salvarTodos(produtoService.extrairProdutos(dadosCombustivelDTO));
		revendaService.salvarTodos(revendaService.extrairRevendas(dadosCombustivelDTO));
		produtoService.salvarLevantamentoComercial(produtoService.extrairLevantamentos(dadosCombustivelDTO));

		return ResponseEntity.status(HttpStatus.OK).body("ok");
	}
}
