package com.example.api.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "produto_revenda")
public class LevantamentoComercialCombustivel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "date_coleta")
	private LocalDate dataColeta;
	
	@Column (name = "valor_compra")
	private BigDecimal valorCompra;
	
	@Column (name = "valor_venda")
	private BigDecimal valorVenda;
	
	@Column (name = "unidade_medida")
	private String unidadeMedida;
	
	@ManyToOne
	@JoinColumn(name = "produto")
	private Produto produto;
	
	@ManyToOne
	@JoinColumn(name = "revenda")
	private Revenda revenda;
	
	@ManyToOne
	@JoinColumn(name = "bandeira")
	private Bandeira bandeira;

	public LevantamentoComercialCombustivel(LocalDate dataColeta, BigDecimal valorCompra, BigDecimal valorVenda,
			String unidadeMedida, Produto produto, Revenda revenda, Bandeira bandeira) {
		this.dataColeta = dataColeta;
		this.valorCompra = valorCompra;
		this.valorVenda = valorVenda;
		this.unidadeMedida = unidadeMedida;
		this.produto = produto;
		this.revenda = revenda;
		this.bandeira = bandeira;
	}

	public LocalDate getDataColeta() {
		return dataColeta;
	}

	public void setDataColeta(LocalDate dataColeta) {
		this.dataColeta = dataColeta;
	}

	public BigDecimal getValorCompra() {
		return valorCompra;
	}

	public void setValorCompra(BigDecimal valorCompra) {
		this.valorCompra = valorCompra;
	}

	public BigDecimal getValorVenda() {
		return valorVenda;
	}

	public void setValorVenda(BigDecimal valorVenda) {
		this.valorVenda = valorVenda;
	}

	public String getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Revenda getRevenda() {
		return revenda;
	}

	public void setRevenda(Revenda revenda) {
		this.revenda = revenda;
	}

	public Bandeira getBandeira() {
		return bandeira;
	}

	public void setBandeira(Bandeira bandeira) {
		this.bandeira = bandeira;
	}

	public Long getId() {
		return id;
	}
}
