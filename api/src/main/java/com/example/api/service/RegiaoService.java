package com.example.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.api.dto.TabelaComercialCombustivelDTO;
import com.example.api.model.Regiao;
import com.example.api.repository.interfaces.RegiaoRepositoryInterface;

@Service
public class RegiaoService {
	
	@Autowired
	protected RegiaoRepositoryInterface regiaoRepository;
	
	/**
	 * Metodo responsável por extrair as regioes do DTO
	 * @param List<TabelaComercialCombustivelDTO> dadosTabelaComercialDTO
	 * @return List<Regiao>
	 */
	public List<Regiao> extrairRegioes(List<TabelaComercialCombustivelDTO> dadosTabelaComercialDTO) {
		List<Regiao> regioes = new ArrayList<Regiao>();
		
		for (TabelaComercialCombustivelDTO dadoComercialDTO : dadosTabelaComercialDTO) {
			Regiao regiao = new Regiao(dadoComercialDTO.getRegiao());
			
			if (!regioes.contains(regiao)) {
				regioes.add(regiao);
			}
		}
		
		return regioes;
	}
	
	/**
	 * Metodo responsável por salvar a regiao
	 *  
	 * @param Regiao regiao
	 * @return Regiao
	 */
	public List<Regiao> salvarTodos(List<Regiao> regiao) {
		return this.regiaoRepository.saveAll(regiao);
	}
	
	public Regiao getEstadoByNome(String sigla) {
		return this.regiaoRepository.findOneBySigla(sigla);
	}
}
