package com.example.api.service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.api.dto.TabelaComercialCombustivelDTO;

@Service
public class ArquivoService {
	public List<TabelaComercialCombustivelDTO> extrairDados(MultipartFile file) throws IOException {
	    File convFile = new File(System.getProperty("java.io.tmpdir")+"/"+ file.getOriginalFilename());
	    file.transferTo(convFile);
	    
	    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");

	    return Files
	    .lines(Paths.get(convFile.getPath()), StandardCharsets.ISO_8859_1)
	    .skip(1)
	    .map(line -> line.replaceAll(",", ".").split("\t"))
	    .map(col -> new TabelaComercialCombustivelDTO(
	    		col[0],
	    		col[1],
	    		col[2],
	    		col[3],
	    		col[4],
	    		LocalDate.parse(col[5], formatter),
	    		col[6].isEmpty() == true ? BigDecimal.ZERO : new BigDecimal(col[6]),
	    		col[7].isEmpty() == true ? BigDecimal.ZERO : new BigDecimal(col[7]),
	    		col[8],
	    		col[9]))
	    .collect(ArrayList<TabelaComercialCombustivelDTO>::new, ArrayList::add, ArrayList::addAll);
	}
}
