package com.example.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.api.dto.TabelaComercialCombustivelDTO;
import com.example.api.model.Bandeira;
import com.example.api.repository.interfaces.BandeiraRepositoryInterface;

@Service
public class BandeiraService {
	@Autowired
	protected BandeiraRepositoryInterface bandeiraRepository;
	
	/**
	 * Metodo responsável por extrair as bandeiras do DTO
	 * @param List<TabelaComercialCombustivelDTO> dadosTabelaComercialDTO
	 * @return List<Bandeira>
	 */
	public List<Bandeira> extrairBandeiras(List<TabelaComercialCombustivelDTO> dadosTabelaComercialDTO) {
		List<Bandeira> bandeiras = new ArrayList<Bandeira>();
		
		for (TabelaComercialCombustivelDTO dadoComercialDTO : dadosTabelaComercialDTO) {
			Bandeira bandeira = new Bandeira(dadoComercialDTO.getBandeira());
			
			if (!bandeiras.contains(bandeira)) {
				bandeiras.add(bandeira);
			}
		}
		
		return bandeiras;
	}
	
	/**
	 * Metodo responsável por salvar a regiao
	 *  
	 * @param Bandeira regiao
	 * @return Bandeira
	 */
	public List<Bandeira> salvarTodos(List<Bandeira> bandeira) {
		return this.bandeiraRepository.saveAll(bandeira);
	}
	
	public Bandeira getOneByNome(String nome) {
		return this.bandeiraRepository.findOneByNome(nome);
	}
}
