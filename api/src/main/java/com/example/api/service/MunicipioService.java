package com.example.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.api.dto.TabelaComercialCombustivelDTO;
import com.example.api.model.Municipio;
import com.example.api.repository.interfaces.MunicipioRepositoryInterface;

@Service
public class MunicipioService {
	@Autowired
	protected MunicipioRepositoryInterface municipioRepository;
	
	@Autowired
	protected EstadoService estadoService;
	
	/**
	 * Metodo responsável por extrair os regioes do DTO
	 * @param List<TabelaComercialCombustivelDTO> dadosTabelaComercialDTO
	 * @return List<Regiao>
	 */
	public List<Municipio> extrairMunicipios(List<TabelaComercialCombustivelDTO> dadosTabelaComercialDTO) {
		List<Municipio> municipios = new ArrayList<Municipio>();
		
		for (TabelaComercialCombustivelDTO dadoComercialDTO : dadosTabelaComercialDTO) {
			Municipio municipio = new Municipio(dadoComercialDTO.getMunicipio(), this.estadoService.getOneBySigla(dadoComercialDTO.getEstado()));
			
			if (!municipios.contains(municipio)) {
				municipios.add(municipio);
			}
		}
		
		return municipios;
	}
	
	/**
	 * Metodo responsável por salvar os municipios
	 *  
	 * @param Municipio municipio
	 * @return Municipio
	 */
	public List<Municipio> salvarTodos(List<Municipio> municipios) {
		return this.municipioRepository.saveAll(municipios);
	}
	
	public Municipio getOneByNome(String nome) {
		return this.municipioRepository.findOneByNome(nome);
	}
}
