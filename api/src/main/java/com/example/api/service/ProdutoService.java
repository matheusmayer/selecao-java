package com.example.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.api.dto.TabelaComercialCombustivelDTO;
import com.example.api.model.LevantamentoComercialCombustivel;
import com.example.api.model.Produto;
import com.example.api.repository.interfaces.LevantamentoComercialCombustivelInterface;
import com.example.api.repository.interfaces.ProdutoRepositoryInterface;

@Service
public class ProdutoService {
	@Autowired
	protected ProdutoRepositoryInterface produtoRepository;
	
	@Autowired
	protected BandeiraService bandeiraService;
	
	@Autowired
	protected RevendaService revendaService;
	
	@Autowired
	protected LevantamentoComercialCombustivelInterface levantamentoComercialRepository;
	
	/**
	 * Metodo responsável por extrair os produtos do DTO
	 * @param List<TabelaComercialCombustivelDTO> dadosTabelaComercialDTO
	 * @return List<Produto>
	 */
	public List<Produto> extrairProdutos(List<TabelaComercialCombustivelDTO> dadosTabelaComercialDTO) {
		List<Produto> produtos = new ArrayList<Produto>();
		
		for (TabelaComercialCombustivelDTO dadoComercialDTO : dadosTabelaComercialDTO) {
			Produto produto = new Produto(dadoComercialDTO.getProduto());
			
			if (!produtos.contains(produto)) {
				produtos.add(produto);
			}
		}
		
		return produtos;
	}
	
	/**
	 * Metodo responsável por extrair os produtos do DTO
	 * @param List<TabelaComercialCombustivelDTO> dadosTabelaComercialDTO
	 * @return List<LevantamentoComercialCombustivel>
	 */
	public List<LevantamentoComercialCombustivel> extrairLevantamentos(List<TabelaComercialCombustivelDTO> dadosTabelaComercialDTO) {
		List<LevantamentoComercialCombustivel> levantamentos = new ArrayList<LevantamentoComercialCombustivel>();
		
		for (TabelaComercialCombustivelDTO dadoComercialDTO : dadosTabelaComercialDTO) {
			LevantamentoComercialCombustivel LevantamentoComercial = new LevantamentoComercialCombustivel(
					dadoComercialDTO.getDataColeta(),
					dadoComercialDTO.getValorCompra(),
					dadoComercialDTO.getValorVenda(),
					dadoComercialDTO.getUnidadeMedida(),
					this.getOneByNome(dadoComercialDTO.getProduto()),
					this.revendaService.getOneByNome(dadoComercialDTO.getRevenda()),
				 	this.bandeiraService.getOneByNome(dadoComercialDTO.getBandeira())
			);
			
			levantamentos.add(LevantamentoComercial);
		}
		
		return levantamentos;
	}
	
	/**
	 * Metodo responsável por salvar os produtos
	 *  
	 * @param Produto produto
	 * @return Produto
	 */
	public List<Produto> salvarTodos(List<Produto> produto) {
		return this.produtoRepository.saveAll(produto);
	}
	
	public Produto getOneByNome(String nome) {
		return this.produtoRepository.findOneByNome(nome);
	}
	
	/**
	 * Metodo responsável por salvar os produtos
	 *  
	 * @param LevantamentoComercialCombustivel produto
	 * @return LevantamentoComercialCombustivel
	 */
	public List<LevantamentoComercialCombustivel> salvarLevantamentoComercial(List<LevantamentoComercialCombustivel> levantamentoComercial) {
		return this.levantamentoComercialRepository.saveAll(levantamentoComercial);
	}
}
