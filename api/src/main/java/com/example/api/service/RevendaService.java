package com.example.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.api.dto.TabelaComercialCombustivelDTO;
import com.example.api.model.Revenda;
import com.example.api.repository.interfaces.RevendaRepositoryInterface;

@Service
public class RevendaService {
	@Autowired
	protected RevendaRepositoryInterface revendaRepository;
	
	@Autowired
	protected MunicipioService municipioService;
	
	/**
	 * Metodo responsável por extrair as revendas do DTO
	 * @param List<TabelaComercialCombustivelDTO> dadosTabelaComercialDTO
	 * @return List<Revenda>
	 */
	public List<Revenda> extrairRevendas(List<TabelaComercialCombustivelDTO> dadosTabelaComercialDTO) {
		List<Revenda> revendas = new ArrayList<Revenda>();
		
		for (TabelaComercialCombustivelDTO dadoComercialDTO : dadosTabelaComercialDTO) {
			Revenda revenda = new Revenda(dadoComercialDTO.getRevenda(), this.municipioService.getOneByNome(dadoComercialDTO.getMunicipio()));
			
			if (!revendas.contains(revenda)) {
				revendas.add(revenda);
			}
		}
		
		return revendas;
	}
	
	/**
	 * Metodo responsável por salvar as revendas
	 *  
	 * @param Revenda revenda
	 * @return Revenda
	 */
	public List<Revenda> salvarTodos(List<Revenda> revendas) {
		return this.revendaRepository.saveAll(revendas);
	}
	
	public Revenda getOneByNome(String nome) {
		return this.revendaRepository.findOneByNome(nome);
	}
}
