package com.example.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.api.dto.TabelaComercialCombustivelDTO;
import com.example.api.model.Estado;
import com.example.api.repository.interfaces.EstadoRepositoryInterface;

@Service
public class EstadoService {
	@Autowired
	protected EstadoRepositoryInterface estadoRepository;
	
	@Autowired
	protected RegiaoService regiaoService;
	
	/**
	 * Metodo responsável por extrair os estados do DTO
	 * @param List<TabelaComercialCombustivelDTO> dadosTabelaComercialDTO
	 * @return List<Estado>
	 */
	public List<Estado> extrairEstados(List<TabelaComercialCombustivelDTO> dadosTabelaComercialDTO) {
		List<Estado> estados = new ArrayList<Estado>();
		
		for (TabelaComercialCombustivelDTO dadoComercialDTO : dadosTabelaComercialDTO) {
			Estado estado = new Estado(dadoComercialDTO.getEstado(), this.regiaoService.getEstadoByNome(dadoComercialDTO.getRegiao()));
			
			if (!estados.contains(estado)) {
				estados.add(estado);
			}
		}
		
		return estados;
	}
	
	/**
	 * Metodo responsável por salvar todos os estados
	 *  
	 * @param Estado estado
	 * @return List<Estado>
	 */
	public List<Estado> salvarTodos(List<Estado> estado) {
		return this.estadoRepository.saveAll(estado);
	}
	
	public Estado getOneBySigla(String sigla) {
		return this.estadoRepository.findOneBySigla(sigla);
	}
}
